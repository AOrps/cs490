1. Instructor can enter questions (including test cases) into question bank.

1. Instructor can select questions from question bank to make exam. This is where points are assigned to questions.

1. Student can take exam.

1. Instructor triggers autograding. Then, can review results, tweak scores and add comments. The instructor can then release the results.

1. Student can review final results, including comments.

**All pieces (or just front->back)?**